<?php

$this->view->options->disableLayout = true;

try {
    // Verify video is available
    if (empty($_GET['vid'])) throw new Exception();

    // Validate given video
    $videoMapper = new VideoMapper();
    $idColumn = (is_numeric ($_GET['vid'])) ? 'video_id' : 'private_url';
    $video = $videoMapper->getVideoByCustom(array(
        $idColumn => $_GET['vid'],
        'status' => 'approved'
    ));
    if (!$video) throw new Exception();
} catch (Exception $e) {
    $video = null;
}

$this->view->vars->webmEncodingEnabled = (Settings::get('webm_encoding_enabled') == '1') ? true : false;
$this->view->vars->theoraEncodingEnabled = (Settings::get('theora_encoding_enabled') == '1') ? true : false;
$this->view->vars->video = $video;

if ($video && !$video->disableEmbed) {
	$video->views++;
	$videoMapper->save($video);
}
